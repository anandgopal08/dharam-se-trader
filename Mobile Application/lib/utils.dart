import 'dart:ui';

import 'package:flutter/material.dart';

//primary color Dark Blue
Color defaultColor = Color(0xFF182641);

//accent color Cyan
Color accentColor = Color(0xFF163163);

//error color Red
Color errorColor = Color(0xFFD50000);

//Default text style with non-white background
TextStyle defaultTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 18,
  fontFamily: 'Roboto'
);

//style for places where text is big and bold
final bigBoldTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 24,
  fontWeight: FontWeight.bold,
  letterSpacing: 1.3,
  fontFamily: 'Roboto',
);

final defaultTextFormFieldStyle = new InputDecoration(
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: defaultColor),
    borderRadius: BorderRadius.circular(11),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: errorColor),
    borderRadius: BorderRadius.circular(11),
  ),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(11),
  ),
  labelStyle: defaultTextStyle.copyWith(color: Colors.black),
);