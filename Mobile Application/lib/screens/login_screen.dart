import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../utils.dart';
import '../widgets/login_form.dart';
import '../screens/signup_screen.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login-screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: height,
                color: defaultColor,
              ),
              Container(
                height: height,
                child: Column(
                  children: [
                    SizedBox(height: height * 0.02),
                    Container(
                      padding: EdgeInsets.only(left: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Welcome Back!',
                            style: bigBoldTextStyle.copyWith(
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text(
                            'Sign in to continue',
                            style: bigBoldTextStyle.copyWith(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            height: height * 0.35,
                            padding: EdgeInsets.only(
                                left: width * 0.25, right: width * 0.03),
                            child: SvgPicture.asset(
                              'assets/images/growth.svg',
                              height: height * 0.35,
                              color: accentColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: height * 0.52,
                      width: width * 0.95,
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(19),
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(30),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  RaisedButton(
                                    onPressed: () {},
                                    elevation: 10,
                                    color: Color(0xFF1AA260),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(11),
                                    ),
                                    child: Container(
                                      height: 51,
                                      width: 120,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          SvgPicture.asset(
                                            'assets/images/google.svg',
                                            height: 30,
                                            width: 18,
                                            color: Colors.white,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            'Sign in',
                                            style: defaultTextStyle.copyWith(
                                                fontSize: 20),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  RaisedButton(
                                    onPressed: () {},
                                    elevation: 10,
                                    color: Color(0xFF3B5998),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(11),
                                    ),
                                    child: Container(
                                      height: 51,
                                      width: 120,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          SvgPicture.asset(
                                              'assets/images/facebook-icon-white.svg',
                                              height: 32,
                                              width: 20,
                                              color: Colors.white),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            'Sign in',
                                            style: defaultTextStyle.copyWith(
                                                fontSize: 20),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            LoginForm(
                              height,
                              width,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Dont have an account?',
                                  style: defaultTextStyle.copyWith(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                                Container(
                                  child: FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pushReplacementNamed(SignUpScreen.routeName);
                                    },
                                    child: Text(
                                      'Sign Up',
                                      style: defaultTextStyle.copyWith(
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF5744D0),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
