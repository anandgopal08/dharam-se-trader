import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import './login_screen.dart';
import '../utils.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    new Future.delayed(
        const Duration(seconds: 5),
        () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()),
            ));
  }

  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: height,
                color: defaultColor,
              ),
              Column(
                children: [
                  SizedBox(height: height * 0.07),
                  Container(
                    child: SvgPicture.asset(
                        'assets/images/dst-logo.svg',height: height * 0.50),
                  ),
                  Container(
                    child: SvgPicture.asset(
                      'assets/images/growth.svg',
                      height: height * 0.40,
                      color: accentColor,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
