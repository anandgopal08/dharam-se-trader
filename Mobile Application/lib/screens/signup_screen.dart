import 'package:DST_App/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../utils.dart';
import '../widgets/signup_form.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = '/signup-screen';
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: height,
                color: defaultColor,
              ),
              Container(
                height: height,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: height * 0.10),
                    Container(
                      padding: EdgeInsets.only(left: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Let\'s Get Started!',
                            style: bigBoldTextStyle.copyWith(
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text(
                            'Create an Account',
                            style: bigBoldTextStyle.copyWith(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    height: height * 0.25,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 12),
                    height: height * 0.70,
                    width: width * 0.97,
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(19),
                      ),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                RaisedButton(
                                  onPressed: () {},
                                  elevation: 10,
                                  color: Color(0xFF1AA260),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: Container(
                                    height: 51,
                                    width: 120,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SvgPicture.asset(
                                          'assets/images/google.svg',
                                          height: 30,
                                          width: 18,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          'Sign Up',
                                          style: defaultTextStyle.copyWith(
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                RaisedButton(
                                  onPressed: () {},
                                  elevation: 10,
                                  color: Color(0xFF3B5998),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: Container(
                                    height: 51,
                                    width: 120,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SvgPicture.asset(
                                            'assets/images/facebook-icon-white.svg',
                                            height: 32,
                                            width: 20,
                                            color: Colors.white),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          'Sign Up',
                                          style: defaultTextStyle.copyWith(
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SignUpForm(height, width),
                          SizedBox(
                              height: height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Already Have an Account?',
                                  style: defaultTextStyle.copyWith(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                                Container(
                                  child: FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
                                    },
                                    child: Text(
                                      'Sign in',
                                      style: defaultTextStyle.copyWith(
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF5744D0),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
