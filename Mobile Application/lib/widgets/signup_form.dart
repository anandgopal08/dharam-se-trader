import 'package:flutter/material.dart';

import '../utils.dart';

class SignUpForm extends StatefulWidget {
  var height;
  var width;

  SignUpForm(
    this.height,
    this.width,
  );
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _username = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  TextEditingController _name = new TextEditingController();
  TextEditingController _phoneNo = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _name,
              decoration: defaultTextFormFieldStyle.copyWith(
                  labelText: 'Name'),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _username,
              decoration:
                  defaultTextFormFieldStyle.copyWith(labelText: 'E-mail'),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _password,
              decoration:
                  defaultTextFormFieldStyle.copyWith(labelText: 'Password'),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _phoneNo,
              decoration:
                  defaultTextFormFieldStyle.copyWith(labelText: 'Phone Number'),
            ),
          ),
          SizedBox(height: widget.height * 0.05,),
          Container(
            height: 49,
            width: 159,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(11),
              ),
              onPressed: () {},
              color: defaultColor,
              child: Text(
                'Sign Up',
                textAlign: TextAlign.center,
                style: defaultTextStyle.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
