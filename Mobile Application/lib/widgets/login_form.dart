import 'package:flutter/material.dart';

import '../utils.dart';

class LoginForm extends StatefulWidget {
  var height;
  var width;

  LoginForm(
    this.height,
    this.width,
  );

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _username = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _username,
              decoration: defaultTextFormFieldStyle.copyWith(
                  labelText: 'Email Address'),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: widget.width * 0.80,
            child: TextFormField(
              controller: _password,
              decoration:
                  defaultTextFormFieldStyle.copyWith(labelText: 'Password'),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 10),
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: () {},
              child: Text(
                'Forgot Password?',
                style: defaultTextStyle.copyWith(
                  color: Color(0xFF5744D0),
                  fontSize: 18,
                ),
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: 49,
            width: 159,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(11),
              ),
              onPressed: () {},
              color: defaultColor,
              child: Text(
                'Sign in',
                textAlign: TextAlign.center,
                style: defaultTextStyle.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
